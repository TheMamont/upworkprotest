//
//  PDPlace.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDPlace.h"
#import "PDPlaceRecord.h"

@interface PDPlace()

@property (nonatomic, readwrite, strong) CLLocation* location;
@property (nonatomic, readwrite, strong) NSString* uid;
@property (nonatomic, readwrite, strong) NSString* name;

@end

@implementation PDPlace

- (id)initWithLocation:(CLLocation*)aLocation uid:(NSString*)aUid name:(NSString*)aName
{
    if (self = [super init])
    {
        self.location = aLocation;
        self.uid = aUid;
        self.name = aName;
    }
    return self;
}

- (id)initWithRecord:(PDPlaceRecord*)aRecord
{
    if (self = [super init])
    {
        self.location = [[CLLocation alloc] initWithLatitude:[aRecord.latitude doubleValue] longitude:[aRecord.longitude doubleValue]];
        self.uid = aRecord.uid;
        self.name = aRecord.name;
    }
    return self;
}

- (void)writeToRecord:(PDPlaceRecord*)aRecord
{
    if (aRecord == nil)
        return;
    
    aRecord.latitude = @(self.location.coordinate.latitude);
    aRecord.longitude = @(self.location.coordinate.longitude);
    aRecord.uid = self.uid;
    aRecord.name = self.name;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"\"%@\" at %.2f %.2f", self.name,
            (float)self.location.coordinate.latitude,
            (float)self.location.coordinate.longitude];
}

@end
