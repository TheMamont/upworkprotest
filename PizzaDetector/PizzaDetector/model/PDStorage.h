//
//  PDStorage.h
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^PDStorageWriteCompletion)(NSError* error);

@class CLLocation;

@interface PDStorage : NSObject

@property (nonatomic, readonly, strong) NSArray* allPlaces;

- (void)writePlaces:(NSArray*)aPlaces completion:(PDStorageWriteCompletion)aCompletion;
- (NSArray*)allPlaces;
- (NSArray*)placesInRange:(double)aRange fromLocation:(CLLocation*)aLocation;

@end
