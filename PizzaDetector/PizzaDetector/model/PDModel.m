//
//  PDModel.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDModel.h"
#import "FourSquareKit.h"
#import "PDStorage.h"
#import "PDLocation.h"
#import "PDPlace.h"
#import "Reachability.h"

#define kClientID       @"BBQFH3ZOKUL1AWSNEMNQGPZMVLEO1DYT3C3S4IZAV0HAKLHN"
#define kClientSecret       @"5XJHK33SMZZMSME1CBYDGEU2YLKZFW0SJHMC4MFLONQGZUR0"
#define kCallbackURL    @"tmtestapp://foursquare"

static double kPDDefaultSearchRange = 1000.0;
NSString * const kPDPlacesDidLoad = @"places-did-load";

@interface PDModel()

@property (nonatomic, readonly, strong) UXRFourSquareNetworkingEngine* foursquare;
@property (nonatomic, readwrite, strong) PDStorage* storage;
@property (nonatomic, readwrite, strong) Reachability* reachability;

@end

@implementation PDModel

+ (PDModel*)model
{
    static PDModel* gInstance = nil;
    static dispatch_once_t gToken;
    dispatch_once(&gToken, ^{
        gInstance = [[PDModel alloc] init];
    });
    return gInstance;
}

- (id)init
{
    if (self = [super init])
    {
        [UXRFourSquareNetworkingEngine registerFourSquareEngineWithClientId:kClientID
                                                                  andSecret:kClientSecret
                                                             andCallBackURL:kCallbackURL];
        self.storage = [[PDStorage alloc] init];
        [self reachability];
    }
    return self;
}

- (UXRFourSquareNetworkingEngine*)foursquare
{
    return [UXRFourSquareNetworkingEngine sharedInstance];
}

- (Reachability*)reachability
{
    return [Reachability reachabilityForInternetConnection];
}

- (void)getRestarauntsForCurrentLocation
{
    __weak typeof (self) weakSelf = self;
    CLLocation* currentLocation = [[PDLocation location] current];
    
    if ([self.reachability currentReachabilityStatus] == NotReachable)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf deliverRestarauntsInRange:kPDDefaultSearchRange from:currentLocation error:nil];
        });
        return;
    }
    
    [self.foursquare exploreRestaurantsNearLatLong:[currentLocation coordinate]
                                         withQuery:@"pizza"
                               withCompletionBlock:^(NSArray *restaurants) {
                                   NSMutableArray* places = [[NSMutableArray alloc] init];
                                   for (UXRFourSquareRestaurantModel* rest in restaurants)
                                   {
                                       CLLocation* loc = [[CLLocation alloc] initWithLatitude:[rest.location.lat doubleValue]
                                                                                    longitude:[rest.location.lng doubleValue]];
                                       PDPlace* place = [[PDPlace alloc] initWithLocation:loc uid:rest.restaurantId name:rest.name];
                                       [places addObject:place];
                                   }
                                   [weakSelf.storage writePlaces:places completion:^(NSError *error) {
                                       [weakSelf deliverRestarauntsInRange:kPDDefaultSearchRange from:currentLocation error:nil];
                                   }];
                               } failureBlock:^(NSError *error) {
                                   [weakSelf deliverRestarauntsInRange:kPDDefaultSearchRange from:currentLocation error:error];
                               }];
}

- (void)deliverRestarauntsInRange:(double)aRange from:(CLLocation*)aLocation error:(NSError*)aError
{
    NSArray* result = [self.storage placesInRange:aRange fromLocation:aLocation];
    NSMutableDictionary* ui = [[NSMutableDictionary alloc] init];
    if (result.count > 0)
        [ui setObject:result forKey:@"places"];
    if (aError > 0)
        [ui setObject:aError forKey:@"error"];
    if (aLocation)
        [ui setObject:aLocation forKey:@"location"];
    [ui setObject:@(aRange) forKey:@"range"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDPlacesDidLoad object:nil userInfo:ui];
}

@end
