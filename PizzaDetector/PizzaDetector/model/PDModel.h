//
//  PDModel.h
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kPDPlacesDidLoad;

@interface PDModel : NSObject

+ (PDModel*)model;
- (void)getRestarauntsForCurrentLocation;

@end
