//
//  PDLocation.h
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern NSString * const kPDLocationServiceDidUpdate;
extern NSString * const kPDLocationServiceDidReachAccuracy;

@class CLLocation;

@interface PDLocation : NSObject

@property (nonatomic, readonly, strong) CLLocation* current;
@property (nonatomic, readonly, assign) BOOL accurate;

+ (PDLocation*)location;

@end
