//
//  PDStorage.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDStorage.h"
#import "MagicalRecord.h"
#import "PDPlace.h"
#import "PDPlaceRecord.h"

@interface PDStorage()

@property (atomic, readwrite, strong) NSArray* places;

@end

@implementation PDStorage

- (id)init
{
    if (self = [super init])
    {
        [self initStorage];
        
        NSArray* allRcords = [PDPlaceRecord MR_findAll];
        NSMutableArray* arr = [[NSMutableArray alloc] init];
        for (PDPlaceRecord* record in allRcords)
            [arr addObject:[[PDPlace alloc] initWithRecord:record]];
        self.places = arr;

    }
    return self;
}

- (void)initStorage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* dbFolder = [[paths lastObject] stringByAppendingPathComponent:@"database"];
    NSFileManager* fm = [NSFileManager defaultManager];
    
    BOOL isDir = YES;
    BOOL cleanupFirst = NO;
    if ([fm fileExistsAtPath:dbFolder isDirectory:&isDir])
    {
        if (!isDir)
            cleanupFirst = YES;
    }
    
    if (cleanupFirst)
    {
        NSError* error = nil;
        [fm removeItemAtPath:dbFolder error:&error];
        if (error != nil)
        {
            NSLog(@"%@", error);
        }
    }
    
    if (![fm fileExistsAtPath:dbFolder isDirectory:nil])
    {
        NSError* error = nil;
        [fm createDirectoryAtPath:dbFolder withIntermediateDirectories:YES attributes:nil error:&error];
        if (error != nil)
        {
            NSLog(@"%@", error);
        }
    }
    
    NSString* dbPath = [dbFolder stringByAppendingPathComponent:@"db.sqlite"];
    NSURL* dbURL = [NSURL fileURLWithPath:dbPath];
    
    [MagicalRecord setDefaultModelNamed:@"Model.momd"];
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreAtURL:dbURL];
}

- (void)writePlaces:(NSArray*)aPlaces completion:(PDStorageWriteCompletion)aCompletion
{
    if (aPlaces.count == 0)
    {
        if (aCompletion != nil)
            aCompletion(nil);
        return;
    }
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext)
     {
         
         NSMutableArray* uids = [NSMutableArray array];
         for (PDPlace* place in aPlaces)
             [uids addObject:place.uid];
         
         NSPredicate* predicate = [NSPredicate predicateWithFormat:@"uid IN %@", uids];
         NSArray* records = [PDPlaceRecord MR_findAllWithPredicate:predicate inContext:localContext];
         
         NSMutableDictionary* recordByUid = [[NSMutableDictionary alloc] initWithCapacity:records.count];
         for (PDPlaceRecord* rec in records)
             [recordByUid setObject:rec forKey:rec.uid];
         
         for (PDPlace* place in aPlaces)
         {
             PDPlaceRecord* rec = [recordByUid objectForKey:place.uid];
             if (rec == nil)
                 rec = [PDPlaceRecord MR_createEntityInContext:localContext];
             
             [place writeToRecord:rec];
         }
     }
                      completion:^(BOOL contextDidSave, NSError *error)
     {
         NSArray* allRcords = [PDPlaceRecord MR_findAll];
         NSMutableArray* arr = [[NSMutableArray alloc] init];
         for (PDPlaceRecord* record in allRcords)
             [arr addObject:[[PDPlace alloc] initWithRecord:record]];
         self.places = arr;

         if (aCompletion != nil)
         {
             dispatch_async(dispatch_get_main_queue(), ^
             {
                 if (contextDidSave)
                     aCompletion(nil);
                 else
                     aCompletion(error);
             });
         }
     }];
}

- (NSArray*)placesInRange:(double)aRange fromLocation:(CLLocation*)aLocation
{
    NSMutableArray* result = [[NSMutableArray alloc] init];
    
    for (PDPlace* place in self.places)
    {
        double dist = [place.location distanceFromLocation:aLocation];
        if (dist < aRange)
        {
            [result addObject:place];
            place.userData = @{@"distance":@(dist)};
        }
    }
    
    [result sortUsingComparator:^NSComparisonResult(PDPlace* p1, PDPlace* p2) {
        double d1 = [p1.location distanceFromLocation:aLocation];
        double d2 = [p2.location distanceFromLocation:aLocation];
        return (d1 < d2) ? (NSOrderedAscending) : (NSOrderedDescending);
    }];
    
    return result;
}

- (NSArray*)allPlaces
{
    return self.places;
}

@end
