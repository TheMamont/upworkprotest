//
//  PDLocation.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDLocation.h"

NSString * const kPDLocationServiceDidUpdate = @"location-did-updated";
NSString * const kPDLocationServiceDidReachAccuracy = @"location-is-accurate";

static double kPDAcceptibleAccuracy = 500.0;

@interface PDLocation()<CLLocationManagerDelegate>

@property (nonatomic, readwrite, strong) CLLocationManager* manager;
@property (nonatomic, readwrite, strong) CLLocation* current;
@property (nonatomic, readwrite, assign) BOOL accurate;

@end

@implementation PDLocation

+ (PDLocation*)location
{
    static PDLocation* gInstance = nil;
    static dispatch_once_t gToken;
    dispatch_once(&gToken, ^{
        gInstance = [[PDLocation alloc] init];
    });
    return gInstance;
}

- (id)init
{
    if (self = [super init])
    {
        self.manager = [[CLLocationManager alloc] init];
        self.manager.delegate = self;
        self.manager.distanceFilter = 25;
        self.manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        [self.manager startUpdatingLocation];
    }
    return self;
}

- (void)dealloc
{
    
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    self.current = newLocation;

    CLLocationDistance accuracy = 0.5 * (self.current.horizontalAccuracy + self.current.verticalAccuracy);

    if (accuracy < kPDAcceptibleAccuracy && !self.accurate)
    {
        self.accurate = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kPDLocationServiceDidReachAccuracy
                                                            object:nil
                                                          userInfo:nil];
    }
    
    NSDictionary* d = [NSDictionary dictionaryWithObject:self.current forKey:@"location"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDLocationServiceDidUpdate
                                                        object:nil
                                                      userInfo:d];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSDictionary* d = @{};
    if (error != nil)
        d = [NSDictionary dictionaryWithObject:error forKey:@"error"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kPDLocationServiceDidUpdate
                                                        object:nil
                                                      userInfo:d];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusNotDetermined)
        [manager requestWhenInUseAuthorization];
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager {}
- (void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager {}

@end
