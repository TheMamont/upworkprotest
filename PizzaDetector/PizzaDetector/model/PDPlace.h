//
//  PDPlace.h
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class PDPlaceRecord;

@interface PDPlace : NSObject

@property (nonatomic, readonly, strong) CLLocation* location;
@property (nonatomic, readonly, strong) NSString* uid;
@property (nonatomic, readonly, strong) NSString* name;

@property (nonatomic, readwrite, strong) NSDictionary* userData;

- (id)initWithLocation:(CLLocation*)aLocation uid:(NSString*)aUid name:(NSString*)aName;
- (id)initWithRecord:(PDPlaceRecord*)aRecord;

- (void)writeToRecord:(PDPlaceRecord*)aRecord;

@end
