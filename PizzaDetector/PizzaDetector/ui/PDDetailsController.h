//
//  PDDetailsController.h
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PDPlace;

@interface PDDetailsController : UIViewController

@property (nonatomic, readwrite, strong) PDPlace* place;

@end
