//
//  PDPlaceTableCell.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDPlaceTableCell.h"
#import "PDPlace.h"

@interface PDPlaceTableCell()

@property (nonatomic, readwrite, strong) IBOutlet UILabel* name;
@property (nonatomic, readwrite, strong) IBOutlet UILabel* distance;

@property (nonatomic, readwrite, strong) PDPlace* place;

@end

@implementation PDPlaceTableCell

+ (PDPlaceTableCell*)createNew
{
    return [[[NSBundle mainBundle] loadNibNamed:@"PDPlaceTableCell"owner:nil options:nil] lastObject];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        
    }
    return self;
}

- (void)dealloc
{
    
}

- (void)fillWithPlace:(PDPlace*)aPlace
{
    self.name.text = @"";
    self.distance.text = @"";
    self.place = aPlace;
    
    if (self.place == nil)
        return;
    
    self.name.text = aPlace.name;
    NSNumber* distNum = [aPlace.userData objectForKey:@"distance"];
    if (distNum != nil)
    {
        int dist = [distNum intValue];
        self.distance.text = [NSString stringWithFormat:@"%dm", dist];
    }
}

@end
