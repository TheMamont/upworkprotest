//
//  PDPlaceTableCell.h
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class PDPlace;

@interface PDPlaceTableCell : UITableViewCell

+ (PDPlaceTableCell*)createNew;
- (void)fillWithPlace:(PDPlace*)aPlace;

@end
