//
//  PDDetailsController.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDDetailsController.h"
#import "PDPlace.h"

static NSString* const kPDCellKey = @"k";
static NSString* const kPDCellValue = @"v";

@interface PDDetailsController() <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, readwrite, strong) IBOutlet UITableView* table;
@property (nonatomic, readwrite, strong) NSArray* content;

@end

@implementation PDDetailsController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self applyPlace];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

NSString* locToStr(float aLoc)
{
    return [NSString stringWithFormat:@"%.2f", aLoc];
}

- (void)applyPlace
{
    self.content = nil;
    if (self.place == nil)
        return;
    
    NSMutableArray* arr = [NSMutableArray array];
    [arr addObject:@{kPDCellKey:@"Name", kPDCellValue:self.place.name}];
    [arr addObject:@{kPDCellKey:@"Latitude", kPDCellValue:locToStr(self.place.location.coordinate.latitude)}];
    [arr addObject:@{kPDCellKey:@"Longitude", kPDCellValue:locToStr(self.place.location.coordinate.longitude)}];
    if (self.place.userData[@"distance"] != nil)
        [arr addObject:@{kPDCellKey:@"Distance (m)", kPDCellValue:@([self.place.userData[@"distance"] intValue])}];
    self.content = arr;
    [self.table reloadData];
    
    self.navigationItem.title = self.place.name;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.content.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* const kReuseId = @"SomeCell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kReuseId];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kReuseId];
    
    NSDictionary* rowData = self.content[indexPath.row];
    cell.textLabel.text = rowData[kPDCellKey];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", rowData[kPDCellValue]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 22;
}

@end
