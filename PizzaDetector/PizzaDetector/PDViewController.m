//
//  ViewController.m
//  PizzaDetector
//
//  Created by Oleksander Mamchych on 7/17/15.
//  Copyright (c) 2015 TundraMobile. All rights reserved.
//

#import "PDViewController.h"
#import "PDModel.h"
#import "PDLocation.h"
#import "PDPlaceTableCell.h"
#import "PDDetailsController.h"

@interface PDViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, readwrite, strong) IBOutlet UITableView* table;
@property (nonatomic, readwrite, strong) IBOutlet UIView* overlay;
@property (nonatomic, readwrite, strong) IBOutlet UILabel* loadingMessage;
@property (nonatomic, readwrite, strong) IBOutlet UIActivityIndicatorView* indicator;

@property (nonatomic, readwrite, strong) NSArray* content;
@property (nonatomic, readwrite, assign) BOOL byName;

- (IBAction)onRefrech:(id)sender;

@end

@implementation PDViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onAccuracyReached:)
                                                 name:kPDLocationServiceDidReachAccuracy
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onPlacesLoaded:)
                                                 name:kPDPlacesDidLoad
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([[PDLocation location] accurate])
    {
        if (self.content == nil)
            [self loadContent];
    }
    else
    {
        [self showLoading:@"Getting location..."];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)onAccuracyReached:(NSNotification*)n
{
    [self loadContent];
}

- (void)onPlacesLoaded:(NSNotification*)n
{
    [self hideLoading];
    NSDictionary* ui = [n userInfo];
    self.content = ui[@"places"];
    self.content = [self.content sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
    [self.table reloadData];
}

- (void)loadContent
{
    [self showLoading:@"Loading..."];
    [[PDModel model] getRestarauntsForCurrentLocation];
}

- (void)showLoading:(NSString*)aMessage
{
    [self.loadingMessage setText:aMessage];
    [self.overlay setHidden:NO];
    [self.indicator startAnimating];
}

- (void)hideLoading
{
    [self.indicator stopAnimating];
    [self.overlay setHidden:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.content.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* const kReuseId = @"PDPlaceTableCell";
    
    PDPlaceTableCell* cell = [tableView dequeueReusableCellWithIdentifier:kReuseId];
    if (cell == nil)
        cell = [PDPlaceTableCell createNew];
    
    PDPlace* place = [self.content objectAtIndex:indexPath.row];
    [cell fillWithPlace:place];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDDetailsController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PDDetailsController"];
    vc.place = [self.content objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onRefrech:(id)sender
{
    [self loadContent];
}

@end
