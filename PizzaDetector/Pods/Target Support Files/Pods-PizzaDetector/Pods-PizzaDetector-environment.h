
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// FourSquareKit
#define COCOAPODS_POD_AVAILABLE_FourSquareKit
#define COCOAPODS_VERSION_MAJOR_FourSquareKit 0
#define COCOAPODS_VERSION_MINOR_FourSquareKit 0
#define COCOAPODS_VERSION_PATCH_FourSquareKit 5

// MKNetworkKit
#define COCOAPODS_POD_AVAILABLE_MKNetworkKit
#define COCOAPODS_VERSION_MAJOR_MKNetworkKit 0
#define COCOAPODS_VERSION_MINOR_MKNetworkKit 87
#define COCOAPODS_VERSION_PATCH_MKNetworkKit 0

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 3
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

